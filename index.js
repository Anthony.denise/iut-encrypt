'use strict';


const crypto = require('crypto');
function encrypt (password) {
    const sha1 = crypto.createHash('sha1');
    return sha1.update(password).digest('hex');
}
module.exports = { encrypt };